/**
 *  Configuration file for wiring of smartBraceletC module to other common 
 *  components needed for proper functioning
 *
 *  @author Luca Pietro Borsani
 */

#include "smartBracelet.h"

configuration smartBraceletAppC {}

implementation {

  components MainC, smartBraceletC as App;
  components new AMSenderC(AM_MY_MSG);
  components new AMReceiverC(AM_MY_MSG);
  components ActiveMessageC;
  components new TimerMilliC();
  components new TimerMilliC() as InfoTimer;
  components new TimerMilliC() as MissingTimer;
  components new FakeSensorC();

  components SerialActiveMessageC as SerialAM;

  //Boot interface
  App.Boot -> MainC.Boot;

  //Send and Receive interfaces
  App.Receive -> AMReceiverC;
  App.AMSend -> AMSenderC;

  //Radio Control
  App.SplitControl -> ActiveMessageC;

  //Interfaces to access package fields
  App.AMPacket -> AMSenderC;
  App.Packet -> AMSenderC;
  App.PacketAcknowledgements->ActiveMessageC;

  //Timer interface
  App.MilliTimer -> TimerMilliC;
  App.InfoTimer -> InfoTimer;
  App.MissingTimer -> MissingTimer;

  //Fake Sensor read
  App.Read3 -> FakeSensorC;

  //Serial
  App.SerialPacket -> SerialAM;
  App.SerialAMSend -> SerialAM.AMSend[AM_SERIAL_MSG];
  App.SerialControl -> SerialAM;
}

