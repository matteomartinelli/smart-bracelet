#ifndef SMARTBRACELET_H
#define SMARTBRACELET_H

#define KEY_LENGTH	20

#define TYPE_KEY_REQ		1
#define TYPE_KEY_RES		2
#define TYPE_INFO			3

#define STATUS_NOT_PAIRED	1
#define STATUS_IN_PAIRING	2
#define STATUS_PAIRED		3

#define NODE_PARENT	1
#define NODE_CHILD	2

#define KIN_STANDING	1
#define KIN_WALKING		2
#define KIN_RUNNING		3
#define KIN_FALLING		4

typedef nx_struct pairing_msg {
	nx_uint8_t	type;
	nx_uint8_t	key[KEY_LENGTH];
} pairing_msg_t;

typedef nx_struct info_msg {
	nx_uint8_t	type;
	nx_uint16_t x;
	nx_uint16_t y;
	nx_uint16_t kinematic;
} info_msg_t;

enum{
AM_MY_MSG = 6,
};

typedef nx_struct serial_msg {
nx_uint16_t serial_value;
} serial_msg_t;

enum {
AM_SERIAL_MSG = 0x89,
};

#define SERIAL_MISSING_ALARM	112
#define SERIAL_FALLING_ALARM	118

#endif
