/**
 *  Source file for implementation of module Middleware
 *  which provides the main logic for middleware message management
 *
 *  @author Luca Pietro Borsani
 */
 
generic module FakeSensorP() {

	provides interface Read3<uint16_t>;
	
	uses interface Random;
	uses interface Timer<TMilli> as Timer0;

} implementation {

	//***************** Boot interface ********************//
	command error_t Read3.read(){
		call Timer0.startOneShot( 10 );
		return SUCCESS;
	}

	//***************** Timer0 interface ********************//
	event void Timer0.fired() {
		uint16_t kinematic = call Random.rand16();
		uint16_t tmp_random = call Random.rand16();
		tmp_random = (tmp_random % 10) + 1;
		if(tmp_random <= 3){
			kinematic = 1;
		}else if (tmp_random <= 6){
			kinematic = 2;
		}else if (tmp_random <= 9){
			kinematic = 3;
		}else {
			kinematic = 4;
		}

		signal Read3.readDone( SUCCESS, call Random.rand16(), call Random.rand16(), kinematic );
	}
}
