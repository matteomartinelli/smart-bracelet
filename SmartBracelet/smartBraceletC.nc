/**
 *  Source file for implementation of module smartBraceletC in which
 *  the node 1 send a request to node 2 until it receives a response.
 *  The reply message contains a reading from the Fake Sensor.
 *
 *  @author Luca Pietro Borsani
 */

#include "smartBracelet.h"
#include "Timer.h"

module smartBraceletC {

  uses {
	interface Boot;
	interface AMPacket;

	interface Packet;
    interface Packet as SerialPacket;

	interface PacketAcknowledgements;

	interface AMSend;
    interface AMSend as SerialAMSend; 

	interface SplitControl;
    interface SplitControl as SerialControl;

	interface Receive;
    interface Timer<TMilli> as MilliTimer;
    interface Timer<TMilli> as InfoTimer;
    interface Timer<TMilli> as MissingTimer;
	interface Read3<uint16_t>;
  }

} implementation {

  uint8_t MY_KEY[KEY_LENGTH];
  uint8_t STATUS;
  uint8_t confirmAddress = 0;
  uint8_t i;
  message_t packet;
  info_msg_t* last_message;

  bool locked = FALSE; //check is serial is busy or not
  message_t serial_packet;

  task void sendPairingKey();
  task void sendConfirmation();
  task void sendInfo();

  //***************** Task send pairing key ********************//
  task void sendPairingKey() {
	pairing_msg_t* mess=(pairing_msg_t*)(call Packet.getPayload(&packet,sizeof(pairing_msg_t)));
	mess->type = TYPE_KEY_REQ;
    for (i=0; i<KEY_LENGTH; i++){
         mess->key[i] = MY_KEY[i];
    }
	    
    dbg_clear("radio_send", "\n");
	dbg("radio_send", "REQUEST: Try to send the key in BROADCAST at time %s \n", sim_time_string());
    
    if(call AMSend.send(AM_BROADCAST_ADDR,&packet,sizeof(pairing_msg_t)) == SUCCESS){
	  dbg("radio_send", "Packet passed to lower layer successfully!\n");
	  dbg("radio_pack",">>>Pack\n \t Payload length %hhu \n", call Packet.payloadLength( &packet ) );
	  dbg_clear("radio_pack","\t Source: %hhu \n ", call AMPacket.source( &packet ) );
	  dbg_clear("radio_pack","\t Destination: %hhu \n ", call AMPacket.destination( &packet ) );
	  dbg_clear("radio_pack","\t AM Type: %hhu \n ", call AMPacket.type( &packet ) );
      dbg_clear("radio_pack","\t\t Payload \n" );
      dbg_clear("radio_pack", "\t\t type: %hhu \n ", mess->type);
      dbg_clear("radio_pack", "\t\t key: ");

      for (i=0; i<KEY_LENGTH; i++){
        dbg_clear("radio_pack","%hhu", mess->key[i]);
      }
      
      dbg_clear("radio_pack", "\n");      
	  dbg_clear("radio_send", "\n ");
	  dbg_clear("radio_pack", "\n");
      
      }
  }

  //****************** Task send response *****************//
  task void sendConfirmation() {
    pairing_msg_t* mess=(pairing_msg_t*)(call Packet.getPayload(&packet,sizeof(pairing_msg_t)));
    mess->type = TYPE_KEY_RES;
    for (i=0; i<KEY_LENGTH; i++){
         mess->key[i] = MY_KEY[i];
    }

    dbg_clear("radio_send", "\n");
    dbg("radio_send", "CONFIRMATION: Try to send the confirmation in UNICAST mode at time %s \n", sim_time_string());
    

    call PacketAcknowledgements.requestAck( &packet );

    if(call AMSend.send(confirmAddress,&packet,sizeof(pairing_msg_t)) == SUCCESS){
      dbg("radio_send", "Packet passed to lower layer successfully!\n");
      dbg("radio_pack",">>>Pack\n \t Payload length %hhu \n", call Packet.payloadLength( &packet ) );
      dbg_clear("radio_pack","\t Source: %hhu \n ", call AMPacket.source( &packet ) );
      dbg_clear("radio_pack","\t Destination: %hhu \n ", call AMPacket.destination( &packet ) );
      dbg_clear("radio_pack","\t AM Type: %hhu \n ", call AMPacket.type( &packet ) );
      dbg_clear("radio_pack","\t\t Payload \n" );
      dbg_clear("radio_pack", "\t\t type: %hhu \n ", mess->type);
      dbg_clear("radio_pack", "\t\t key: ");

      for (i=0; i<KEY_LENGTH; i++){
        dbg_clear("radio_pack","%hhu", mess->key[i]);
      }

      dbg_clear("radio_pack", "\n");      
      dbg_clear("radio_send", "\n ");
      dbg_clear("radio_pack", "\n");
      
      }
  }

  task void sendInfo() {
    call Read3.read();
  }

  //***************** Boot interface ********************//
  event void Boot.booted() {
	dbg("boot","Application booted.\n");

	//Genero la chiave in modo statico
	for (i=0; i<KEY_LENGTH; i++){
		 MY_KEY[i] = i%10;
	}

	//Setto il mio status come "not_paired"
	STATUS = STATUS_NOT_PAIRED;

	call SplitControl.start();
  }

  //***************** SplitControl interface ********************//
  event void SplitControl.startDone(error_t err){
    if(err == SUCCESS) {
		dbg("radio","Radio on!\n");
		//Indipendentemente che sia TOS_NODE_ID == 1 o TOS_NODE_ID == 2 mando in broadcast la mia key
		dbg("role","I'm node %hhu, I will start sending my pairing key in BROADCAST every 500ms\n", TOS_NODE_ID);
        call SerialControl.start();
		call MilliTimer.startPeriodic( 500 );
    }else{
	  call SplitControl.start();
    }

  }
  
  event void SplitControl.stopDone(error_t err){}

  event void SerialControl.startDone(error_t err) {
    if(err == SUCCESS) {
      dbg("boot", "I'm the node %d and the Serial is ON! \n", TOS_NODE_ID);
    }
  }

  event void SerialControl.stopDone(error_t err) {}

  //***************** MilliTimer interface ********************//
  event void MilliTimer.fired() {
    post sendPairingKey();
  }
  

  //***************** MilliTimer interface ********************//
  event void MissingTimer.fired() {
    //Se viene firato questo timer è perchè il braccialetto è andato fuori range e stampo un messaggio
    dbg("alarm", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
    dbg("alarm", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
    dbg("alarm", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
    dbg("alarm", "@@@@@                                                       @@@@@@\n");
    dbg("alarm", "@@@@@        !!ALARM!! The child is missing !!ALARM!!       @@@@@@\n");
    dbg("alarm", "@@@@@                          x: %hhu                        @@@@@@\n", last_message->x);
    dbg("alarm", "@@@@@                          y: %hhu                        @@@@@@\n", last_message->y);
    dbg("alarm", "@@@@@                      kinematic: %hhu                     @@@@@@\n", last_message->kinematic);
    dbg("alarm", "@@@@@                                                       @@@@@@\n");
    dbg("alarm", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
    dbg("alarm", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
    dbg("alarm", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");

	//Code for send Serial packet in case of crisis
	if (locked) {
		return;
	}else {
		serial_msg_t* rcm = (serial_msg_t*)call SerialPacket.getPayload(&serial_packet, sizeof(serial_msg_t));
		if (rcm == NULL) {return;}
		if (call SerialPacket.maxPayloadLength() < sizeof(serial_msg_t)) {
			return;
		}
		rcm->serial_value = SERIAL_MISSING_ALARM;
		if (call SerialAMSend.send(AM_BROADCAST_ADDR, &serial_packet, sizeof(serial_msg_t)) == SUCCESS) {
			locked = TRUE;
			dbg("init","Packet sent over serial port...\n");
		}
	}

  }

  //***************** MilliTimer interface ********************//
  event void InfoTimer.fired() {
    post sendInfo();
  }

  //********************* AMSend interface ****************//
  event void AMSend.sendDone(message_t* buf,error_t err) {
    pairing_msg_t* mess=(pairing_msg_t*)(call Packet.getPayload(&packet,sizeof(pairing_msg_t)));

    if(&packet == buf && err == SUCCESS ) {
		dbg("radio_send", "Packet sent...");

        if(mess->type == TYPE_KEY_RES){
            if ( call PacketAcknowledgements.wasAcked( buf ) ) {
              dbg_clear("radio_ack", "and ack received");
              //Se ricevo ACK allora ho completato il pairing quindi
              STATUS=STATUS_PAIRED;
              call MilliTimer.stop();

              if(TOS_NODE_ID == NODE_CHILD){
                  call InfoTimer.startPeriodic(10000);
              }else if (TOS_NODE_ID == NODE_PARENT){
                  call MissingTimer.startOneShot(60000);
              }
            } else {
              dbg_clear("radio_ack", "but ack was not received");
              post sendConfirmation();
            }
        }
        
		dbg_clear("radio_send", " at time %s \n", sim_time_string());
    }

  }

  //***************************** Receive interface *****************//
  event message_t* Receive.receive(message_t* buf,void* payload, uint8_t len) {
	info_msg_t* mess=(info_msg_t*)payload;
    uint8_t error = 0;

	if(mess->type == TYPE_INFO){
        dbg_clear("radio_rec", "\n");
        dbg("radio_rec","RECEIVED-INFO: Message received at time %s \n", sim_time_string());
        dbg("radio_pack",">>>Pack \n \t Payload length %hhu \n", call Packet.payloadLength( buf ) );
        dbg_clear("radio_pack","\t Source: %hhu \n", call AMPacket.source( buf ) );
        dbg_clear("radio_pack","\t Destination: %hhu \n", call AMPacket.destination( buf ) );
        dbg_clear("radio_pack","\t AM Type: %hhu \n", call AMPacket.type( buf ) );

        dbg_clear("radio_pack","\t\t Payload \n" );
        dbg_clear("radio_pack", "\t\t type: %hhu \n", mess->type);
        dbg_clear("radio_pack", "\t\t x: %hhu \n", mess->x);
        dbg_clear("radio_pack", "\t\t y: %hhu \n", mess->y);
        dbg_clear("radio_pack", "\t\t kinematic: %hhu \n", mess->kinematic);
        dbg_clear("radio_rec", "\n ");
        dbg_clear("radio_pack","\n");

		//Ho ricevuto un messaggio di INFO
		//Controllo che sono un parent altrimenti lo discardo e controllo che sono in STATUS_PAIRED
        if(TOS_NODE_ID == NODE_PARENT && STATUS == STATUS_PAIRED){
            call MissingTimer.stop();

            last_message = mess;

            if(mess->kinematic == KIN_FALLING){
                //dbg_clear("radio_send", "arrivo qui e il kinemtica e: %hhu.\n", mess->kinematic);
                dbg_clear("alarm", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
                dbg_clear("alarm", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
                dbg_clear("alarm", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
                dbg_clear("alarm", "@@@@@                                                       @@@@@@\n");
                dbg_clear("alarm", "@@@@@        !!ALARM!! The child is falling !!ALARM!!       @@@@@@\n");
                dbg_clear("alarm", "@@@@@                                                       @@@@@@\n");
                dbg_clear("alarm", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
                dbg_clear("alarm", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
                dbg_clear("alarm", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n\n");

				//Code for send Serial packet in case of crisis
				if (locked) {
					return;
				}else {
					serial_msg_t* rcm = (serial_msg_t*)call SerialPacket.getPayload(&serial_packet, sizeof(serial_msg_t));
					if (rcm == NULL) {return;}
					if (call SerialPacket.maxPayloadLength() < sizeof(serial_msg_t)) {
						return;
					}
					rcm->serial_value = SERIAL_FALLING_ALARM;
					if (call SerialAMSend.send(AM_BROADCAST_ADDR, &serial_packet, sizeof(serial_msg_t)) == SUCCESS) {
						locked = TRUE;
						dbg("init","Packet sent over serial port...\n");
					}
				}

            }

            call MissingTimer.startOneShot(60000);
        }
	}else{
        pairing_msg_t* pair_msg=(pairing_msg_t*)payload;

		//Ho ricevuto un messaggio di PAIRING
        dbg_clear("radio_rec", "\n");
        dbg("radio_rec","RECEIVED-PAIRING: Message received at time %s \n", sim_time_string());
        dbg("radio_pack",">>>Pack \n \t Payload length %hhu \n", call Packet.payloadLength( buf ) );
        dbg_clear("radio_pack","\t Source: %hhu \n", call AMPacket.source( buf ) );
        dbg_clear("radio_pack","\t Destination: %hhu \n", call AMPacket.destination( buf ) );
        dbg_clear("radio_pack","\t AM Type: %hhu \n", call AMPacket.type( buf ) );

        dbg_clear("radio_pack","\t\t Payload \n" );
        dbg_clear("radio_pack", "\t\t type: %hhu \n", pair_msg->type);
        dbg_clear("radio_pack", "\t\t key: ");

        for (i=0; i<KEY_LENGTH; i++){
            dbg_clear("radio_pack","%hhu", pair_msg->key[i]);
        }

        dbg_clear("radio_pack", "\n");

        dbg_clear("radio_rec", "\n ");
        dbg_clear("radio_pack","\n");

        if(STATUS != STATUS_PAIRED){
            //Potrebbe essere una conferma o una richiesta
            if(pair_msg->type == TYPE_KEY_REQ){
                //Qualcuno mi ha fatto una richiesta, controllo la chiave e mando conferma che deve essere ACKappata
                for (i=0; i<KEY_LENGTH; i++){
                     if(pair_msg->key[i] != MY_KEY[i]) error = 1;
                }

                if(error == 0){
                    STATUS = STATUS_IN_PAIRING;
                    confirmAddress = call AMPacket.source( buf );
                    post sendConfirmation();
                }else{
                    STATUS = STATUS_NOT_PAIRED;
                    //Key errata, non fare nulla
                }
                
            }else{
                //Se è una conferma vol dire che avevo mandato una REQ e ora mi hanno mandato una conferma RES
                for (i=0; i<KEY_LENGTH; i++){
                     if(pair_msg->key[i] != MY_KEY[i]) error = 1;
                }

                if(error == 0){
                    //dbg("pairing", "Pairing completed");
                    STATUS = STATUS_PAIRED;
                    call MilliTimer.stop();

                    if(TOS_NODE_ID == NODE_CHILD){
                        call InfoTimer.startPeriodic(10000);
                    }else if (TOS_NODE_ID == NODE_PARENT){
                        call MissingTimer.startOneShot(60000);
                    }
                }else{
                    STATUS = STATUS_NOT_PAIRED;
                    //Key errata, non fare nulla
                }
            }
        }
	}
	
    return buf;

  }
  
  event void SerialAMSend.sendDone(message_t* bufPtr, error_t error) {
    if(&serial_packet == bufPtr) {
      dbg("serial_send","Packet sent over serial...");
      locked=FALSE;
    }
  }
  //************************* Read interface **********************//
  event void Read3.readDone(error_t result, uint16_t data_x, uint16_t data_y, uint16_t data_kinematic) {
    info_msg_t* mess=(info_msg_t*)(call Packet.getPayload(&packet,sizeof(info_msg_t)));
    mess->type = TYPE_INFO;
    mess->x = data_x;
    mess->y = data_y;
    mess->kinematic = data_kinematic;

    dbg_clear("radio_send", "\n");
    dbg("radio_send", "INFO: Try to send a info messagge to parent at time %s \n", sim_time_string());

    if(call AMSend.send(NODE_PARENT,&packet,sizeof(info_msg_t)) == SUCCESS){
        dbg("radio_send", "Packet passed to lower layer successfully!\n");
        dbg("radio_pack",">>>Pack\n \t Payload length %hhu \n", call Packet.payloadLength( &packet ) );
        dbg_clear("radio_pack","\t Source: %hhu \n ", call AMPacket.source( &packet ) );
        dbg_clear("radio_pack","\t Destination: %hhu \n ", call AMPacket.destination( &packet ) );
        dbg_clear("radio_pack","\t AM Type: %hhu \n ", call AMPacket.type( &packet ) );
        dbg_clear("radio_pack","\t\t Payload \n" );
        dbg_clear("radio_pack", "\t\t type: %hhu \n ", mess->type);
        dbg_clear("radio_pack", "\t\t x: %hhu \n", mess->x);
        dbg_clear("radio_pack", "\t\t y: %hhu \n", mess->y);
        dbg_clear("radio_pack", "\t\t kinematic: %hhu \n", mess->kinematic);
        dbg_clear("radio_send", "\n ");
        dbg_clear("radio_pack", "\n");

    }

  }

}

